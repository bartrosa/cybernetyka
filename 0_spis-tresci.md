# Cybernetyka
## Spis treści
[Wstęp](./1_wstep.md)  
--
[Rozdział 1 - Zarys Historyczny](./rozdzial_1/wstep.md)  
[Rozdział 2 - Logika Aksjomatyczno-Informacyjna](./rozdzial_2/wstep.md)
--
[Słownik](./slownik/0_slownik.md)  
[Postacie](./postacie/0_postacie.md) 
--